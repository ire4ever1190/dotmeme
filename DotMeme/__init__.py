"""
This module creates memes by finding the points and images from .meme files
it's functions are

meme

listMemes
"""
import os
import zipfile
from io import BytesIO

import pkg_resources
import requests
import yaml
from PIL import Image, ImageDraw, ImageFont


def meme(meme_file_path, ):
	"""
	Function that reads a .meme file and create a meme from it with the text specified
	:param meme_file_path:
		The name of the meme or path
	:param points:
		The text for the points
	:param image_name:
		The output name of the image if it to be saved. default is meme.png
	:param save_meme:
		Boolean, to save the meme to a file or not. default is False
	:return:
		Returns the Pillow img object
	"""
	# Checks if the person has .meme on the file specified







class Meme:
	def read(self, meme_file_name, return_image=False):
		if '.meme' in os.path.basename(meme_file_name):
			meme_file_name = os.path.basename(meme_file_name)
		else:
			meme_file_name = os.path.basename(meme_file_name) + '.meme'

		# Checks if the file exists and if it doesn't then it chcks if the package has it
		try:
			open(meme_file_name)
		except FileNotFoundError:
			try:
				meme_file_path = pkg_resources.resource_filename(__name__,
										 'Data/Memes/' + meme_file_name)
			except KeyError:
				raise MemeNotFoundError
			except FileNotFoundError:
				meme_file_path = pkg_resources.resource_filename(__name__,
										 'Data/Memes/' + meme_file_name + '.meme')

		try:
			with open(meme_file_path) as file:
				yml_data = yaml.safe_load(file)
				if return_image:
					response = requests.get(yml_data['image']['url'])
					image = Image.open(BytesIO(response.content))

		# This means that it is a zip file
		except UnicodeDecodeError:
			print(meme_file_path)
			archive = zipfile.ZipFile(meme_file_path, 'r')
			yml_data = archive.read(meme_file_name).decode('utf-8')
			yml_data = yaml.safe_load(yml_data)
			if return_image:
				for file in archive.namelist():
					if ".jpg" in file or ".png" in file:
						image = Image.open(archive.open(file))
						break
		except FileNotFoundError:
			raise MemeNotFoundError

		if return_image:
			return yml_data, image
		else:
			return yml_data

		# Checks that the correct number of points are added

	def write(self, meme_file_name, *points, image_name="meme.png", save_meme=False):
		yml_data, image = self.read(meme_file_name, return_image=True)
		assert len(points) == len(yml_data['text']), f"Incorrect number of points, you have entered text for {len(points)} but it requires {len(yml_data['text'])}"
		draw = ImageDraw.Draw(image)
		point_index = 0
		# Places the text onto the image for each point
		for point in yml_data['text']:
			point1, point2 = yml_data['text'][point]['point'].split()
			# Convert the points from string to int for the position
			point1 = int(point1)
			point2 = int(point2)
			text = points[point_index]
			text_position = (point1, point2)
			font_location = pkg_resources.resource_filename(__name__, 'Data/font.ttf')
			font = ImageFont.truetype(font_location, size=45)
			draw.text(text_position, text, (0, 0, 0), font=font)
			point_index += 1
		if save_meme:
			image.save(image_name, optimize=True)
		return image

	def list(self):
		"""
		A generator that yields all the memes that the package has
		:return:
			yields the meme files with .meme extension
		"""
		for meme_file in pkg_resources.resource_listdir(__name__, 'Data/Memes'):
			yield meme_file


class MemeNotFoundError(Exception):
	""" Raised when a meme file is not found. Usually because it doesn't exist"""
	print("Check that you have the name or path correct for the meme")
