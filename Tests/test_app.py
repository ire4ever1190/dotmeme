from DotMeme import *
import filecmp
import os
import pytest
import pkg_resources

meme = Meme()
print(os.getcwd())
def check_memes(meme_file_name):
	"""Used to check if two memes are similar.  """
	print(meme_file_name)
	assert filecmp.cmp(os.path.join(os.getcwd(), meme_file_name), os.path.join(os.path.dirname(__file__),'ControlImages', meme_file_name))

class TestApp():
	def test_meme_zips(self):
		for zip in meme.list():
			print(zip)
			imageName = zip.split('.')[0]
			imageName += '.png'
			meme.write(zip, 'My dog', 'Me', 'My cat', image_name=imageName, save_meme=True)
			check_memes(imageName)

	def test_no_meme_file(self):
		with pytest.raises(MemeNotFoundError):
			meme.write('ThisShouldFailAndIHopeNobodyMakesADotMemeFileForItBecauseThenIWouldNeedToTestThisADifferentWay', 'Apples')

	def test_incorrect_number_of_points(self):
		try:
			meme.write('DisloyalBoyfriend.meme', 'apples')
		except AssertionError:
			pass
	def test_file_extension(self):
		imageNames = ['DisloyalBoyfriend' , 'DisloyalBoyfriend.meme']
		for imageName in imageNames:
			meme.write(imageName, 'My dog', 'Me', 'My cat', image_name='DisloyalBoyfriend.png', save_meme=True)
			check_memes("DisloyalBoyfriend.png")

