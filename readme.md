the meme file is basically just a yaml file but with .meme extension

it should go like this

```name: disloyal boyfriend
image:
  url: https://i.kym-cdn.com/entries/icons/mobile/000/023/732/damngina.jpg
text:
  point1:
    point: 448 176
  point2:
    point: 94 270
  point3:
    point: 623 209
```

This is a the disloyal boyfriend in .meme format
