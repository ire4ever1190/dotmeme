import setuptools

setuptools.setup(
	name="DotMeme",
	version="0.4.1",
	author="Jake Leahy",
	author_email="darhyaust@gmail.com",
	description="A package to use .meme files",
	url="https://gitlab.com/ire4ever1190/dotmeme",
	packages=setuptools.find_packages(),
	include_package_data=True,
	install_requires=[
		'Pillow',
		'pyyaml',
		'requests'
	],
	classifiers=[
		"Programming Language :: Python :: 3",
		"License :: OSI Approved :: MIT License",
		"Operating System :: OS Independent",
	],
)
